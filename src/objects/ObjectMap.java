package objects;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

import org.openqa.selenium.By;

public class ObjectMap {
	
    Properties properties = new Properties();
    
    //Method to get locator type and locator value
   
    public By getLocator(String strElement) throws Exception {
    	
    	InputStream stream = new FileInputStream(new File(System.getProperty("user.dir")+"\\files\\object.properties"));
    	properties.load(stream);
        String locator = properties.getProperty(strElement);
        String locatorType = locator.split(":")[0];
        String locatorValue = locator.split(":")[1];

        System.out.println("Retrieving object of type '" + locatorType + "' and value '" + locatorValue + "' from the object map");

        if(locatorType.toLowerCase().equals("id"))
            return By.id(locatorValue);
        else if(locatorType.toLowerCase().equals("name"))
            return By.name(locatorValue);
        else if((locatorType.toLowerCase().equals("classname")) || (locatorType.toLowerCase().equals("class")))
            return By.className(locatorValue);
        else if((locatorType.toLowerCase().equals("tagname")) || (locatorType.toLowerCase().equals("tag")))
            return By.className(locatorValue);
        else if((locatorType.toLowerCase().equals("linktext")) || (locatorType.toLowerCase().equals("link")))
            return By.linkText(locatorValue);
        else if((locatorType.toLowerCase().equals("partiallinktext")) || (locatorType.toLowerCase().equals("partiallink")))
            return By.partialLinkText(locatorValue);
        else if((locatorType.toLowerCase().equals("cssselector")) || (locatorType.toLowerCase().equals("css")))
            return By.cssSelector(locatorValue);
        else if(locatorType.toLowerCase().equals("xpath"))
            return By.xpath(locatorValue);
        else
            throw new Exception("Unknown locator type '" + locatorType + "'");
    }
}