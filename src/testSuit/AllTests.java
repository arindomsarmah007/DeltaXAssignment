package testSuit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import prerequisteSetup.Setup_Testing;
import prerequisteSetup.Finish_Testing;
import testCases.Acceptance_Criteria;
import testCases.Sensible_Validations;

@RunWith(Suite.class)
@SuiteClasses({ 
	
	Setup_Testing.class,
	Acceptance_Criteria.class,
	Sensible_Validations.class,
	Finish_Testing.class
	

})
public class AllTests {
	
	public static void main(String[] args) throws Exception {  
		
		org.junit.runner.JUnitCore.main("testSuit.AllTests"); 
	}
}
