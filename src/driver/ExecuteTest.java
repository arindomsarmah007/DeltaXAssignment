package driver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class ExecuteTest {
	
	WebDriver driver=null;
	
	//Method to open Browser and launch URL
	
	public WebDriver openBrowserLaunchUrl() throws IOException, InterruptedException {
		
		InputStream stream=new FileInputStream(new File(System.getProperty("user.dir")+"\\files\\browserURLInfo.properties"));
		Properties properties = new Properties();
		properties.load(stream);
		String browserLaunchUrlInfo = properties.getProperty("browserNameUrlValue");
	    String browserName = browserLaunchUrlInfo.split(" ")[0];
	    String urlValue  = browserLaunchUrlInfo.split(" ")[1];
	    
	    if (browserName.toLowerCase().equals("firefox"))
	    {
	      	System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\softwares\\geckodriver.exe");
	      	driver=new FirefoxDriver();
	    }
	    else if(browserName.toLowerCase().equals("ie"))
	    {
	      	System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\softwares\\IEDriverServer.exe");
	      	driver=new InternetExplorerDriver(); 
	     }
	    else if(browserName.toLowerCase().equals("chrome"))
	    {
	      	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\softwares\\chromedriver.exe");
	    	driver=new ChromeDriver();
	     } 
	    driver.get(urlValue);
	    driver.manage().window().maximize();
	    return driver;
	}
	
	//Base Method to perform action
	
	public void performAction(WebDriver driver,By by,String operation,String value) throws Exception
	{
		this.driver=driver;
		WebElement element=driver.findElement(by);
		switch (operation.toUpperCase())
		{
		case "SETTEXT":
			element.sendKeys(value);
			break;
		case "CLICK":
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", element);
			break;
		}
	}
	
	//Base Method to perform validation
	
	public boolean performValidation(WebDriver driver,By by,String operation,String value)
	{
		this.driver=driver;
		boolean result=false;
		switch (operation.toUpperCase()){
		case "GETTEXT":
			result=driver.findElement(by).getText().trim().equals(value.trim());
			break;
		}
		return result;
	}
}		