package prerequisteSetup;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import excelExportAndFileIO.ReadExcelFile;

public class PrerequisteSetup {
	
	private String Directory = null;
	private static String completeLogFile = null;
	private static LogManager logManager = LogManager.getLogManager();
	private static Logger logger = Logger.getLogger("Log");
	private static FileHandler logFileHandle = null;
	private Date testCaseStartTime = null;
	private Date testCaseEndTime = null;
	private static Date testSetStartTime = null;
	private static Date testSetEndTime = null;
	private WebDriver driver = null;
	private String testCaseName;
	ReadExcelFile ReadData=new ReadExcelFile();
	
	//Method to create log and result directory, log file, system out file, set logger and test set start time	
	
	public void SetupTestSetProperties() throws Exception
	{
		Directory = System.getProperty("user.dir")+"/LogDirectory";
		Date now = new Date();
		SimpleDateFormat dateFormat =  new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		String directory_name = dateFormat.format(now).toString();
		    
		boolean success = (new File(Directory+"/"+directory_name).mkdir());
		if(!success)
		{
		    System.out.println("!!! Error: Could not create Log directory:"+directory_name+" under Log directory :"+Directory);
		    System.out.println("!!! Error: Check if the directory exists, if not create it :"+Directory+" and re-run again");
		    Exception exp = new Exception("Please check if the log directory configured is correct and exists :"+Directory);
		    throw exp;
		}
		    
		String Log_Directory=Directory+"/"+directory_name;		
		System.setProperty("Log_Directory", Log_Directory);
		
		File destDir = new File(Log_Directory);
		File srcFile = new File(System.getProperty("user.dir")+"\\files\\TestCasesResult.xlsx");
		FileUtils.copyFileToDirectory(srcFile, destDir);
		
		String logFileName = "LogFile";
		completeLogFile = Log_Directory+"/"+logFileName+".log";
		File logFile = new File(completeLogFile);
		logFile.createNewFile();
			
        
        String sysoFileName = "SystemOutFile";
	    String sysoFile = Log_Directory+"/"+sysoFileName+".txt";
	    File outPutFile = new File(sysoFile);
	    outPutFile.createNewFile();
        
        PrintStream o = new PrintStream(new File(sysoFile));
        System.setOut(o);
		    
		logManager.reset();
		logger.setLevel(Level.FINE);
	    logManager.addLogger(logger);
	    	
	    logFileName = completeLogFile;
		logFileHandle = new FileHandler(logFileName);
		logFileHandle.setFormatter(new SimpleFormatter());
		logger.addHandler(logFileHandle);
			
		logger.log(Level.FINE,"Log Directory : "+Log_Directory);
		System.out.println("Log Directory : "+Log_Directory);
		logger.log(Level.FINE,"Log file created");
		System.out.println("Log file created");
		logger.log(Level.FINE,"********* Log file : "+completeLogFile+" *********");
		System.out.println("********* Log file : "+completeLogFile+" *********");
		logger.log(Level.FINE,"System Out file created");
		System.out.println("System Out file created");
		logger.log(Level.FINE,"********* SYSO file : "+sysoFile+" *********");
		System.out.println("********* SYSO file : "+sysoFile+" *********");		
	
		testSetStartTime = new Date();
		
		System.out.println("Test Set Started at: "+testSetStartTime.toString());
		logger.log(Level.FINE,"Test Set Started at: "+testSetStartTime.toString()); 
		
		System.setProperty("testSetStartTime", testSetStartTime.toString());   
	}
	
	public void SetTestCaseName(String testCaseName)
	{
			this.testCaseName = testCaseName;
	}
	
	//Method to notify test case start and record test case start time 
	
	public void DisplayStartTestCase(String testCaseName)
	{
		testCaseStartTime = new Date();
		
		logger.log(Level.FINE,"@@@@@@@@@ Starting Test case : "+testCaseName+" @@@@@@@@@");
		System.out.println("@@@@@@@@@ Starting Test case : "+testCaseName+" @@@@@@@@@");
		
		logger.log(Level.FINE,"^^^^^^^^^ Started Test case : "+testCaseName+" : at : "+testCaseStartTime.toString()+" ^^^^^^^^^");
		System.out.println("^^^^^^^^^ Started Test case : "+testCaseName+" : at : "+testCaseStartTime.toString()+" ^^^^^^^^^");
		
		System.setProperty("testCaseStartTime", testCaseStartTime.toString());

	}
	
	//Method to notify test case end and record result, test case end time and total time taken by the test case
	
	public void DisplayEndTestCase(String testCaseName, Boolean result) throws IOException
	{
		String result_string = null;
		testCaseEndTime = new Date();
			
		result_string = (result == true)?"Passed":"Failed";
		logger.log(Level.FINE,"+++++++++ Test case :"+testCaseName+":"+result_string+" +++++++++");
		System.out.println("+++++++++ Test case :"+testCaseName+":"+result_string+" +++++++++");
			
		logger.log(Level.FINE,"######### Finished Test case : "+testCaseName+" : at : "+testCaseEndTime.toString()+" #########");
		System.out.println("######### Finished Test case : "+testCaseName+" : at : "+testCaseEndTime.toString()+" #########");
			
		System.setProperty("TestCaseEndTime", testCaseEndTime.toString());
		
		if (result)
		{
			ReadData.updateColumnData("Result","Passed",testCaseName);
		}
		else
		{
			ReadData.updateColumnData("Result","Failed",testCaseName);
		}
		
		String testCaseStartTimegot = System.getProperty("testCaseStartTime");
		int totalTimeTestCase = 0;
		
		try 
		{
			Date testCaseStart ;
			SimpleDateFormat dateFormat =  new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy");
			testCaseStart = (Date)dateFormat.parse(testCaseStartTimegot);
			totalTimeTestCase= (int) ((testCaseEndTime.getTime() - testCaseStart.getTime())/1000);
		} 
		catch (ParseException e)
		{
			System.out.println("Exception :"+e);
		} 
		
		System.out.println("Total time taken by Test Case: "+totalTimeTestCase+" seconds");
		String totalTime= Integer.toString(totalTimeTestCase);
		ReadData.updateColumnData("TotalTimeTaken",totalTime+" Seconds",testCaseName);
	}
	
	//Method to record exception
		
	public void DisplayErrorTestCase(WebDriver driver, String testCaseName, Exception e) throws Exception
	{
		this.driver=driver;
		SetTestCaseName(testCaseName);
		String directory = System.getProperty("Log_Directory");
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();
		
		logger.log(Level.FINE,"**************Error**************");
		System.out.println("**************Error**************");
		logger.log(Level.FINE,sStackTrace);
		System.out.println(e);
		
		logger.log(Level.FINE,"#########Finished Test case  : "+testCaseName+" : With Error#########");
		System.out.println("#########Finished Test case  : "+testCaseName+" : With Error#########");
		
		ReadData.updateColumnData("Result","An error has occurred.",testCaseName);
		
		takeScreenShot(driver, testCaseName, directory);
	}
	
	//method to notify disabled test case
	
	public void DisabledTestCase(String testCaseName) throws IOException
	{
		SetTestCaseName(testCaseName);
		
		logger.log(Level.FINE,"@@@@@@@@@ Test case Not Enabled : "+testCaseName+" @@@@@@@@@");
		System.out.println("@@@@@@@@@ Test case Not Enabled : "+testCaseName+" @@@@@@@@@");
		
		ReadData.updateColumnData("Result","Test case Not Enabled",testCaseName);
	}
	
	//Method to notify test set end and record total time taken by the test set 
	
	public void FinishTestSet() throws Exception
	{
		String testSetStartTimegot = System.getProperty("testSetStartTime");
		testSetEndTime = new Date();
		
		int totalTimeTestSet = 0;
		try 
		{
			Date testSetStart ;
			SimpleDateFormat dateFormat =  new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy");
			testSetStart = (Date)dateFormat.parse(testSetStartTimegot);
			totalTimeTestSet= (int) ((testSetEndTime.getTime() - testSetStart.getTime())/1000);
		} 
		catch (ParseException e)
		{
			System.out.println("Exception :"+e);
		} 
		
		logger.log(Level.FINE,"Total time taken by Test Set : "+totalTimeTestSet+" seconds");
		System.out.println("Total time taken by Test Set : "+totalTimeTestSet+" seconds");
		System.out.println("$$$$$$$$$$$$$$$$$$$ Completed the entire Test Set $$$$$$$$$$$$$$$$$$$$$$$$");
	}
	
	//Method to take screenshot if exception occurs
	
	public void takeScreenShot(WebDriver driver, String testCaseName,String directory) throws Exception
	{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String filePath= directory+"/"+testCaseName+".png";
        FileUtils.copyFile(scrFile, new File(filePath));
    }
}