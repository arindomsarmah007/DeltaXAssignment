package excelExportAndFileIO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelFile {
	
	//Method to read from excel file
	
    public Sheet readExcel(String filePath,String fileName,String sheetName) throws IOException
    {
    	File file =    new File(filePath+"\\"+fileName);
    	FileInputStream inputStream = new FileInputStream(file);
    	XSSFWorkbook testDataWorkbook = new XSSFWorkbook(inputStream);
    	Sheet testDataSheet = testDataWorkbook.getSheet(sheetName);
    	inputStream.close();
    	return testDataSheet; 
    }
    
    //Method to read test data from excel file

	public String ReadTestData(String colName,String testCaseName) throws IOException
	{
		Properties properties = new Properties();	
		InputStream stream = new FileInputStream(new File(System.getProperty("user.dir")+"\\files\\dataSheetInfo.properties"));
		properties.load(stream);
		String dataInfo = properties.getProperty(testCaseName);
		String workBookName = dataInfo.split(":")[0];
		String workSheetName  = dataInfo.split(":")[1];
		ReadExcelFile file = new ReadExcelFile();
		Sheet testDataSheet = file.readExcel(System.getProperty("user.dir")+"\\files\\",workBookName,workSheetName);
	
		int rowCount = testDataSheet.getLastRowNum()-testDataSheet.getFirstRowNum();
		
		Map<String, Integer> map=new HashMap<String, Integer>();
		
		Row rowNum = testDataSheet.getRow(0);
		for (int j = 0; j< rowNum.getLastCellNum(); j++)
		{
			if (rowNum.getCell(j)!=null)
			{
				if(rowNum.getCell(j).toString().equalsIgnoreCase(colName))
				{
					map.put(rowNum.getCell(j).toString(),j);		
				}	      
			}
		} 
	
		short colNum=Short.parseShort((map.get(colName)).toString());	     
		for (int i = 0; i<rowCount+2; i++)
		{
			Row rowNumber = testDataSheet.getRow(i);
			if (rowNumber.getCell(0)!=null && rowNumber.getCell(0).toString().equals(testCaseName))
			{
				   return rowNumber.getCell(colNum).toString();
			}
			stream.close();		     
		}
		return "Row not found"; 
	}
	
	//Method to write test data in excel file
	
	public void updateColumnData(String columns,String values,String testCaseName) throws IOException {
	
		Properties properties = new Properties();
		InputStream stream = new FileInputStream(new File(System.getProperty("user.dir")+"\\files\\dataSheetInfo.properties"));
		properties.load(stream);
		String dataInfo = properties.getProperty(testCaseName);
		String workBookName = dataInfo.split(":")[2];
		String workSheetName  = dataInfo.split(":")[1];
	
		File file1 =new File(System.getProperty("Log_Directory")+"\\"+workBookName);
	    FileInputStream inputStream = new FileInputStream(file1);
	    Workbook testDataWorkbook = new XSSFWorkbook(inputStream);
	    Sheet testDataSheet = testDataWorkbook.getSheet(workSheetName);
		int rowCount = testDataSheet.getLastRowNum()-testDataSheet.getFirstRowNum();
	
		Map<String, Integer> map=new HashMap<String, Integer>();
		Row rowNum = testDataSheet.getRow(0);
		for (int j = 0; j< rowNum.getLastCellNum(); j++)
		{
			if (rowNum.getCell(j)!=null)
			{
				if(rowNum.getCell(j).toString().equalsIgnoreCase(columns))
				{
					map.put(rowNum.getCell(j).toString(),j);		
				}	      
			}
		}  
	
		short colNum=Short.parseShort((map.get(columns)).toString());
		
		for (int i = 1; i<rowCount+2; i++)
		{
			Row rowNumber = testDataSheet.getRow(i);
			if (rowNumber.getCell(0)!=null && rowNumber.getCell(0).toString().equals(testCaseName))
			{
				Cell cell = rowNumber.createCell(colNum);
				cell.setCellValue(values);
				break;
			}
		}
					    	 	
		inputStream.close();		     
		FileOutputStream outputStream = new FileOutputStream (file1);		  
		testDataWorkbook.write(outputStream);	
		outputStream.close();			  
	}
}