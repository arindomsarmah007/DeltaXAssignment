package testCases;

import objects.ObjectMap;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import prerequisteSetup.PrerequisteSetup;
import driver.ExecuteTest;
import excelExportAndFileIO.ReadExcelFile;

public class Acceptance_Criteria {
	
    @Test 
    public void test() throws Exception{
    	
    	String testName;
    	boolean result;
    	
    	ObjectMap objMap=new ObjectMap();
    	ExecuteTest ExecuteTest=new ExecuteTest();
    	ReadExcelFile ReadData=new ReadExcelFile();
    	PrerequisteSetup prerequisteSetup=new PrerequisteSetup();
    	WebDriver driver = null;
    	
//    	**********************************testCase_101:Empty First Name**********************************
    	
    testName="testCase_101";
    result=false;
    	
    prerequisteSetup.DisplayStartTestCase(testName);
    	
    if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
    {
    	try{
    	
    		driver=ExecuteTest.openBrowserLaunchUrl();
    				
    		ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
    		ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
    		ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
    		ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
    		ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
    		ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
    			
    		result=ExecuteTest.performValidation(driver,objMap.getLocator("firestNameEmptyError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));    				
    		driver.close();
    		prerequisteSetup.DisplayEndTestCase(testName,result);
    		}
    	catch(Exception e){
    		prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
    	}
    }
    else
    {
    	prerequisteSetup.DisabledTestCase(testName);
    }
    
//	**********************************testCase_102:Empty Last Name**********************************
	
	testName="testCase_102";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);	
	
	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
			
			result=ExecuteTest.performValidation(driver,objMap.getLocator("lastNameEmptyError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_103:Empty Username **********************************
	
	testName="testCase_103";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);
	
	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
			
			result=ExecuteTest.performValidation(driver,objMap.getLocator("usernameEmptyError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_104:Empty Password**********************************
	
	testName="testCase_104";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);
	
	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
		
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
			
			result=ExecuteTest.performValidation(driver,objMap.getLocator("passwordEmptyError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_105:Empty Confirm Password **********************************
	
	testName="testCase_105";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
			
			result=ExecuteTest.performValidation(driver,objMap.getLocator("confirmPasswordEmptyError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}

//	**********************************testCase_106:Empty Email Address**********************************
	
	testName="testCase_106";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
			
			result=ExecuteTest.performValidation(driver,objMap.getLocator("emailEmptyError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_107:First Name Minimum Length**********************************
	
	testName="testCase_107";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("firestNameInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_108:Last Name Minimum Length**********************************
	
	testName="testCase_108";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
			
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("lastNameInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_109:Username Minimum Length**********************************
	
	testName="testCase_109";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("usernameInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
		
//	**********************************testCase_110:Password Minimum Length**********************************
	
	testName="testCase_110";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("passwordInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
	
//	**********************************testCase_111:Confirm Password Minimum Length**********************************
	
	testName="testCase_111";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);
	
	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("confirmPasswordInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
	
//	**********************************testCase_112:Email Address Minimum Length**********************************
	
	testName="testCase_112";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);
	
	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("emailInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
		}
			
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_113:Invalid Email Address**********************************
	
	testName="testCase_113";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
			Thread.sleep(2000);	

			result=ExecuteTest.performValidation(driver,objMap.getLocator("emailInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			prerequisteSetup.DisplayEndTestCase(testName,result);
			driver.close();
			}
		catch(Exception e){
			prerequisteSetup.DisplayErrorTestCase(driver,testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
	
//	**********************************testCase_114:Invalid Mobile Number**********************************
	
	testName="testCase_114";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("contactNo"),"SETTEXT",ReadData.ReadTestData("ContactNo",testName));

			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
				
			result=ExecuteTest.performValidation(driver,objMap.getLocator("contactNoInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
			prerequisteSetup.DisplayEndTestCase(testName,result);
			}
		catch(Exception e){
    	    prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
		}
	}
	else
	{
		prerequisteSetup.DisabledTestCase(testName);
	}
  }
}