package testCases;

import driver.ExecuteTest;
import objects.ObjectMap;
import excelExportAndFileIO.ReadExcelFile;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import prerequisteSetup.PrerequisteSetup;

public class Sensible_Validations {
	
    @Test 
    public void test() throws Exception{
	
    	String testName;
    	boolean result;
    	
    	ObjectMap objMap=new ObjectMap();
    	ExecuteTest ExecuteTest=new ExecuteTest();
    	ReadExcelFile ReadData=new ReadExcelFile();
    	PrerequisteSetup prerequisteSetup=new PrerequisteSetup();
    	WebDriver driver = null;
	
	
//	**********************************testCase_201:Special characters in First Name**********************************
	
	testName="testCase_201";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
				
			Thread.sleep(2000);

			result=ExecuteTest.performValidation(driver,objMap.getLocator("firestNameInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
    		prerequisteSetup.DisplayEndTestCase(testName,result);
    		}
    	catch(Exception e){
    		prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
    	}
    }
    else
    {
    	prerequisteSetup.DisabledTestCase(testName);
    }
	
//	**********************************testCase_202:Special characters in Last Name**********************************
	
	testName="testCase_202";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
				
			Thread.sleep(2000);

			result=ExecuteTest.performValidation(driver,objMap.getLocator("lastNameInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
    		prerequisteSetup.DisplayEndTestCase(testName,result);
    		}
    	catch(Exception e){
    		prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
    	}
    }
    else
    {
    	prerequisteSetup.DisabledTestCase(testName);
    }
	
//	**********************************testCase_203:Special characters in Username**********************************
	
	testName="testCase_203";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
				
			Thread.sleep(2000);

			result=ExecuteTest.performValidation(driver,objMap.getLocator("usernameInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
    		prerequisteSetup.DisplayEndTestCase(testName,result);
    		}
    	catch(Exception e){
    		prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
    	}
    }
    else
    {
    	prerequisteSetup.DisabledTestCase(testName);
    }
	
//	**********************************testCase_204:Password and Confirm Password**********************************
	
	testName="testCase_204";
	result=false;
	
	prerequisteSetup.DisplayStartTestCase(testName);

	if (ReadData.ReadTestData("Automated",testName).equals("Y") && ReadData.ReadTestData("Execute",testName).equals("Y"))
	{
		try{
	
			driver=ExecuteTest.openBrowserLaunchUrl();
				
			ExecuteTest.performAction(driver,objMap.getLocator("firstName"),"SETTEXT",ReadData.ReadTestData("FirstName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("lastName"),"SETTEXT",ReadData.ReadTestData("LastName",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("username"),"SETTEXT",ReadData.ReadTestData("Username",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("password"),"SETTEXT",ReadData.ReadTestData("Password",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("confirmPassword"),"SETTEXT",ReadData.ReadTestData("ConfirmPassword",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("email"),"SETTEXT",ReadData.ReadTestData("Email",testName));
			ExecuteTest.performAction(driver,objMap.getLocator("submitBtn"),"CLICK",ReadData.ReadTestData("Automated",testName));	
				
			Thread.sleep(2000);

			result=ExecuteTest.performValidation(driver,objMap.getLocator("confirmPasswordInvalidError"),"GETTEXT",ReadData.ReadTestData("ErrorMessage",testName));
			driver.close();
    		prerequisteSetup.DisplayEndTestCase(testName,result);
    		}
    	catch(Exception e){
    		prerequisteSetup.DisplayErrorTestCase(driver, testName, e);
			driver.close();
    	}
    }
    else
    {
    	prerequisteSetup.DisabledTestCase(testName);
    }

}
}

